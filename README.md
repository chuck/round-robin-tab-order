# Round Robin Tab Order

This extension changes the window-switching order from most-recently-used to just
cycling through all windows in a stable order.
