/* roundrobintaborder: Make the window switch order round-robin instead of most-recently-used
 * Copyright (C) 2019 Scott Worley
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * or version 3 of the License (at your option).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

let oldGetTabList;

// We sort windows as if lexicographically by the key
//     [is_smaller_than_or_equal_to_current_window_id, id].
// This causes the current window to have the key
//     [1, <largest id of all windows that have 1 in the first key slot>]
// and thus to always sort to the end of the list.
function roundRobinComparitor(currentWindow, a, b) {
    const aid = a.get_id(), alec = aid <= currentWindow;
    const bid = b.get_id(), blec = bid <= currentWindow;
    if (alec !== blec) return blec - alec;
    return bid - aid;
}

function init() {
    oldGetTabList = global.display.get_tab_list;
}

function enable() {
    global.display.get_tab_list = function(type, workspace) {
        const ret = oldGetTabList.call(this, type, workspace);
        const currentWindow = global.display.get_focus_window().get_id();
        ret.sort((a, b) => roundRobinComparitor(currentWindow, a, b));
        return ret;
    };
}

function disable() {
    global.display.get_tab_list = oldGetTabList;
}

